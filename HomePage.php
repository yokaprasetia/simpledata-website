<?php
    include "koneksi.php";

    session_start();
    if (isset($_SESSION['user_id'])) {
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/stylesheet.css">
    <link rel="icon" href="gambar/gambar-logo.png">
    <title>SINIMASUK</title>
</head>
<body>
    <div class="content">
        <nav class="main">
            <div class="left-side">
                <ul>
                    <li><a class="menu" href=""><img src="gambar/gambar-menu.png" alt="">MENU</a>
                        <div class="submenu">
                            <ul>
                                <li class="sub-submenu"><a class="menu2" href="">HELP<img src="gambar/gambar-arrow.png" alt=""></a>
                                    <div class="submenu2">
                                        <ul>
                                            <li><a href="AboutUsPage.php">ABOUT US</a></li>
                                            <li><a href="CreditsPage.php">CREDITS</a></li>
                                        </ul>
                                    </div>
                                </li>
                                <li><a href="LogoutPage.php">LOGOUT</a></li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>

            <div class="right-side">
                <ul>
                    <li><a href="HomePage.php">HOME<span class="span-home"></a></li>
                    <li><a href="DataItemPage.php">DATA ITEM<span class="span-home"></a></li>
                    <li><a href="AddItemPage.php">ADD ITEM<span class="span-home"></a></li>
                    <li><a href="EditItemPage.php">EDIT ITEM<span class="span-home"></a></li>
                </ul>
            </div>

            <div class="menu-toggle">
                <input type="checkbox">
                <span></span>
                <span></span>
                <span></span>
            </div>

            <div class="garis"></div>
        </nav>

        <div class="belakang-nav"></div>

<!-- ============================BAGIAN EDIT START============================ -->
        <br>
        <div class="isian-data">
            <div class="home-page1">
                <img src="gambar/gambar-logo.png" alt="">
            </div>

            <div class="home-page2">
                <h1>SUPERMARKET 'SINIMASUK'</h1>
                <h2>"Marketing dan Teknologi yang Menciptakan Kemudahan bagi Pengguna"</h2>
            </div>
        </div>

<!-- ============================BAGIAN EDIT END============================ -->
    </div>

    <div class="bottom-side"></div>
    <div class="footer">
        <p><b>SINIMASUK&copy;2021</b></p>
        <p><b>YokaPrasMT_</b></p>
    </div>
    <script src="javaScript/scriptAll.js"></script>
</body>
</html>

<?php
    } else {
        echo "
        <script>
        alert('Maaf, Login terlebih dahulu...');
        document.location= 'LoginPage.php';
        </script>
        ";
    }
?>