<?php
    include "koneksi.php";

    if(isset($_POST['prosesCreate'])) {

        $user_id = $_POST['user_id'];
        $username = $_POST['username'];
        $email = $_POST['email'];

        $panjang_id = strlen($user_id);
        
        $query = mysqli_query($koneksi, "SELECT * FROM user WHERE user_id = '$user_id' OR email = '$email'");

        $cek = mysqli_num_rows($query);


        // ERROR HANDLING
        if($cek == 1) {
            echo "
            <script>
            alert('Gagal dibuat! User ID atau Email telah digunakan...')
            document.location = 'CreateAccountPage.php';
            </script>
            ";
        } elseif (!preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/", $email)) {
            echo "
            <script>
            alert('Gagal dibuat! Alamat Email tidak ditemukan...')
            document.location = 'CreateAccountPage.php';
            </script>
            ";
        } elseif (!($panjang_id == 9)) {
            echo "
            <script>
            alert('Gagal dibuat! User ID harus 9 digit...')
            document.location = 'CreateAccountPage.php';
            </script>
            ";
        } elseif (!preg_match('/^[0-9\s]+$/', $user_id)) {
            echo "
            <script>
            alert('Gagal dibuat! User ID harus berupa angka...')
            document.location = 'CreateAccountPage.php';
            </script>
            ";
        } else {
            mysqli_query($koneksi, "INSERT INTO user VALUES('$user_id', '$username', '$email')");

            echo "
            <script>
                alert('Akun Berhasil Dibuat!...')
                document.location = 'LoginPage.php';
            </script>
            ";
        }
        
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/create.css">
    <link rel="icon" href="gambar/gambar-logo.png">
    <title>SINIMASUK</title>
</head>
<body>
    <div class="main">
        <div class="isidata">
            <h1>CREATE AN ACCOUNT</h1>
            <br>
            <div class="form">
                <form method="post" action="CreateAccountPage.php">
                <label for="email">Email</label><br />
                <input type="text" name="email" id="email" placeholder="Enter your Email" required><br />
                <label for="username">Username</label><br />
                <input type="text" name="username" id="username" placeholder="Create your Username" required><br />
                <label for="user_id">User ID</label><br />
                <input type="password" id="user_id" name="user_id" placeholder="Create your User ID" required><br />
                <input id="login" type="submit" value="CREATE" name="prosesCreate">
                <label><a href="LoginPage.php">Sudah punya akun? Login di sini</a></label>
                </form>
            </div>
            <br>
        </div>
    </div>

    <div class="bottom-side"></div>
    <div class="footer">
        <p><b>SINIMASUK&copy;2021</b></p>
        <p><b>YokaPrasMT_</b></p>
    </div>
</body>
</html>