<?php
    include "koneksi.php";

    session_start();
    if (isset($_SESSION['user_id'])) {
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/stylesheet.css">
    <link rel="icon" href="gambar/gambar-logo.png">
    <title>SINIMASUK</title>
</head>
<body>
    <div class="content">
        <nav class="main">
            <div class="left-side">
                <ul>
                    <li><a class="menu" href=""><img src="gambar/gambar-menu.png" alt="">MENU</a>
                        <div class="submenu">
                            <ul>
                                <li class="sub-submenu"><a class="menu2" href="">HELP<img src="gambar/gambar-arrow.png" alt=""></a>
                                    <div class="submenu2">
                                        <ul>
                                            <li><a href="AboutUsPage.php">ABOUT US</a></li>
                                            <li><a href="CreditsPage.php">CREDITS</a></li>
                                        </ul>
                                    </div>
                                </li>
                                <li><a href="LogoutPage.php">LOGOUT</a></li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>

            <div class="right-side">
                <ul>
                    <li><a href="HomePage.php">HOME<span class="span-home"></a></li>
                    <li><a href="DataItemPage.php">DATA ITEM<span class="span-home"></a></li>
                    <li><a href="AddItemPage.php">ADD ITEM<span class="span-home"></a></li>
                    <li><a href="EditItemPage.php">EDIT ITEM<span class="span-home"></a></li>
                </ul>
            </div>

            <div class="menu-toggle">
                <input type="checkbox">
                <span></span>
                <span></span>
                <span></span>
            </div>

            <div class="garis"></div>
        </nav>

        <div class="belakang-nav"></div>

<!-- ============================BAGIAN EDIT START============================ -->

        <div class="isian-data">
            <div class="updateItem-page1">
                <h1>DATA BARANG SUPERMARKET 'SINIMASUK'</h1>
                <br>
            </div>
            <div class="updateItem-page2">
                <?php
                    $kode_barang = $_GET['kode_barang'];
                    $data = mysqli_query($koneksi, "SELECT * FROM barang WHERE kode_barang = '$kode_barang'");
                    while ($tampil = mysqli_fetch_array($data)) {
                ?>
                    <form action="ProsesUpdate.php" method="post">
                        <table class="update">
                            <tr>
                                <td width="200"><label for="kode_barang">KODE BARANG</label></td>
                                <td><input type="text" id="kode_barang" name="kode_barang" size="40" value="<?php echo $tampil['kode_barang']; ?>" readonly></td>
                            </tr>
                            <tr>
                                <td><label for="nama_barang">NAMA BARANG</label></td>
                                <td><input type="text" id="nama_barang" name="nama_barang" size="40" value="<?php echo $tampil['nama_barang']; ?>" required></td>
                            </tr>
                            <tr>
                                <td><label for="harga_barang">HARGA BARANG</label></td>
                                <td><input type="text" id="harga_barang" name="harga_barang" size="40" value="<?php echo $tampil['harga_barang']; ?>" required></td>
                            </tr>
                            <tr>
                                <td><label for="stok_barang">STOK BARANG</label></td>
                                <td><input type="text" id="stok_barang" name="stok_barang" size="40" value="<?php echo $tampil['stok_barang']; ?>" required></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td>
                                <input type="submit" name="prosesUpdate" value="UBAH">
                                </td>
                                <td>
                                <input type="reset" name="batal" value="CANCEL">
                                </td>
                                <td>
                                <input type="button" name="kembali" value="KEMBALI" onclick="window.history.back()">
                                </td>
                            </tr>
                        </table>
                    </form>

                <?php
                    }
                ?>
            </div>

            <div class="updateItem-responsive">
                <?php
                    $kode_barang = $_GET['kode_barang'];
                    $data = mysqli_query($koneksi, "SELECT * FROM barang WHERE kode_barang = '$kode_barang'");
                    while ($tampil = mysqli_fetch_array($data)) {
                ?>
                    <form action="ProsesUpdate.php" method="post">
                        <table class="update">
                            <tr><td width="200"><label for="kode_barang">KODE BARANG</label></td></tr>
                            <tr><td><input type="text" id="kode_barang" name="kode_barang" size="40" value="<?php echo $tampil['kode_barang']; ?>" readonly></td></tr>
                            
                            <tr><td></td></tr>
                            <tr><td></td></tr>
                            <tr><td><label for="nama_barang">NAMA BARANG</label></td></tr>
                            <tr><td><input type="text" id="nama_barang" name="nama_barang" size="40" value="<?php echo $tampil['nama_barang']; ?>" required></td></tr>
                            
                            <tr><td></td></tr>
                            <tr><td></td></tr>
                            <tr><td><label for="harga_barang">HARGA BARANG</label></td></tr>
                            <tr><td><input type="text" id="harga_barang" name="harga_barang" size="40" value="<?php echo $tampil['harga_barang']; ?>" required></td></tr>
                            
                            <tr><td></td></tr>
                            <tr><td></td></tr>
                            <tr><td><label for="stok_barang">STOK BARANG</label></td></tr>
                            <tr><td><input type="text" id="stok_barang" name="stok_barang" size="40" value="<?php echo $tampil['stok_barang']; ?>" required></td></tr>
                            
                            <tr><td></td></tr>
                            <tr><td></td></tr>
                            <tr><td><input type="submit" name="prosesUpdate" value="UBAH"></td></tr>
                            <tr><td><input type="reset" name="batal" value="CANCEL"></td></tr>
                            <tr><td><input type="button" name="kembali" value="KEMBALI" onclick="window.history.back()"></td></tr>
                        </table>
                    </form>

                <?php
                    }
                ?>
            </div>
        </div>

<!-- ============================BAGIAN EDIT END============================ -->
    </div>

    <div class="bottom-side"></div>
    <div class="footer">
        <p><b>SINIMASUK&copy;2021</b></p>
        <p><b>YokaPrasMT_</b></p>
    </div>
    <script src="javaScript/scriptAll.js"></script>
</body>
</html>

<?php
    } else {
        echo "
        <script>
        alert('Maaf, Login terlebih dahulu...');
        document.location= 'LoginPage.php';
        </script>
        ";
    }
?>