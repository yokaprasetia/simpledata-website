<?php
    session_start();
    unset($_SESSION['user_id']);
    session_destroy();

    session_start();
    include "koneksi.php";

    if(isset($_POST['prosesLogin'])) {

        $username = $_POST['username'];
        $user_id = $_POST['user_id'];

        $qry = mysqli_query($koneksi, "SELECT * FROM user WHERE user_id = '$user_id' AND username = '$username'");
        $cek = mysqli_num_rows($qry);
        if ($cek == 1) {
            $_SESSION['user_id'] = $user_id;
            echo "
            <script>
            alert('Login Berhasil!...')
            document.location = 'HomePage.php';
            </script>
            ";
        } else {
            echo "
            <script>
            alert('Login Gagal! Username dan User ID tidak sesuai...')
            document.location = 'LoginPage.php';
            </script>
            ";
        }
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/login.css">
    <link rel="icon" href="gambar/gambar-logo.png">
    <title>SINIMASUK</title>
</head>
<body>
    <div class="content">
        <div class="isian-data">
            <div class="login-page1">
                <h1>MENU LOGIN</h1>
                <br>
                <div class="form">
                    <form method="POST" action="LoginPage.php">
                        <label for="username">Username</label><br />
                        <input type="text" name="username" id="username" placeholder="Enter your User ID" required><br />
                        <label for="user_id">User ID</label><br />
                        <input type="password" name="user_id" id="user_id"  placeholder="Enter your ID" required><br />
                        <input id="login" type="submit" value="LOGIN" name="prosesLogin">
                        <label><a href="CreateAccountPage.php">Belum punya akun? Buat di sini</a></label>
                    </form>
                </div>
                <br>
            </div>
        </div>
    </div>

    <div class="bottom-side"></div>
    <div class="footer">
        <p><b>SINIMASUK&copy;2021</b></p>
        <p><b>YokaPrasMT_</b></p>
    </div>
</body>
</html>