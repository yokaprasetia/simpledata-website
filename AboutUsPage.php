<?php
    include "koneksi.php";

    session_start();
    if (isset($_SESSION['user_id'])) {
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/stylesheet.css">
    <link rel="icon" href="gambar/gambar-logo.png">
    <title>SINIMASUK</title>
</head>
<body>
    <div class="content">
        <nav class="main">
            <div class="left-side">
                <ul>
                    <li><a class="menu" href=""><img src="gambar/gambar-menu.png" alt="">MENU</a>
                        <div class="submenu">
                            <ul>
                                <li class="sub-submenu"><a class="menu2" href="">HELP<img src="gambar/gambar-arrow.png" alt=""></a>
                                    <div class="submenu2">
                                        <ul>
                                            <li><a href="AboutUsPage.php">ABOUT US</a></li>
                                            <li><a href="CreditsPage.php">CREDITS</a></li>
                                        </ul>
                                    </div>
                                </li>
                                <li><a href="LogoutPage.php">LOGOUT</a></li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>

            <div class="right-side">
                <ul>
                    <li><a href="HomePage.php">HOME<span class="span-home"></a></li>
                    <li><a href="DataItemPage.php">DATA ITEM<span class="span-home"></a></li>
                    <li><a href="AddItemPage.php">ADD ITEM<span class="span-home"></a></li>
                    <li><a href="EditItemPage.php">EDIT ITEM<span class="span-home"></a></li>
                </ul>
            </div>

            <div class="menu-toggle">
                <input type="checkbox">
                <span></span>
                <span></span>
                <span></span>
            </div>

            <div class="garis"></div>
        </nav>

        <div class="belakang-nav"></div>

<!-- ============================BAGIAN EDIT START============================ -->

        <div class="isian-data">
            <div class="aboutUs-page1">
                <h1>ABOUT US</h1>
                <br>
            </div>
            <div class="aboutUs-page2">
                <table class="about">
                    <tr>
                        <th>Nama</th>
                        <td>Yoka Prasetia</td>
                    </tr>
                    <tr>
                        <th>NIM</th>
                        <td>221910846</td>
                    </tr>
                    <tr>
                        <th>Kelas/Absen</th>
                        <td>2KS1/10</td>
                    </tr>
                    <tr>
                        <th>Mata Kuliah</th>
                        <td>Pemrograman Berbasis Web</td>
                    </tr>
                    <tr>
                        <th>Kampus</th>
                        <td>Politeksik Statistika STIS</td>
                    </tr>
                    <tr>
                        <th>Alamat Kampus</th>
                        <td>Jl. Otto Iskandar Dinata No.64C</td>
                    </tr>
                    <tr>
                        <th>Alamat Domisili</th>
                        <td>Jl. Mangun Harjo, Cangapan Rt.72 Dk.Boto, Patalan Jetis, Bantul, Yogyakarta 55781</td>
                    </tr>
                    <tr>
                        <th>Umur</th>
                        <td>19 Tahun</td>
                    </tr>
                    <tr>
                        <th>Cita- cita</th>
                        <td>Menjadi Statistisi Handal, Berkualitas, serta Berakhlak Mulia</td>
                    </tr>
                    <tr>
                        <th>No. HP</th>
                        <td>+62-8953-7926-1962</td>
                    </tr>
                </table>
            </div>

            <div class="aboutUs-responsive">
                <table class="about">
                    <tr><th>Nama</th></tr>
                    <tr><td>Yoka Prasetia</td></tr>

                    <tr><td class="spasi">spasi</td></tr>
                    
                    <tr><th>NIM</th></tr>
                    <tr><td>221910846</td></tr>

                    <tr><td class="spasi">spasi</td></tr>

                    <tr><th>Kelas/Absen</th></tr>
                    <tr><td>2KS1/10</td></tr>
                    
                    <tr><td class="spasi">spasi</td></tr>

                    <tr><th>Mata Kuliah</th></tr>
                    <tr><td>Pemrograman Berbasis Web</td></tr>

                    <tr><td class="spasi">spasi</td></tr>

                    <tr><th>Kampus</th></tr>
                    <tr><td>Politeksik Statistika STIS</td></tr>
                    
                    <tr><td class="spasi">spasi</td></tr>

                    <tr><th>Alamat Kampus</th></tr>
                    <tr><td>Jl. Otto Iskandar Dinata No.64C</td></tr>

                    <tr><td class="spasi">spasi</td></tr>

                    <tr><th>Alamat Domisili</th></tr>
                    <tr><td>Jl. Mangun Harjo, Cangapan Rt.72 Dk.Boto, Patalan Jetis, Bantul, Yogyakarta 55781</td></tr>

                    <tr><td class="spasi">spasi</td></tr>

                    <tr><th>Umur</th></tr>
                    <tr><td>19 Tahun</td></tr>
                    
                    <tr><td class="spasi">spasi</td></tr>

                    <tr><th>Cita- cita</th></tr>
                    <tr><td>Menjadi Statistisi Handal, Berkualitas, serta Berakhlak Mulia</td></tr>

                    <tr><td class="spasi">spasi</td></tr>

                    <tr><th>No. HP</th></tr>
                    <tr><td>+62-8953-7926-1962</td></tr>
                </table>
            </div>
        </div>

<!-- ============================BAGIAN EDIT END============================ -->
    </div>

    <div class="bottom-side"></div>
    <div class="footer">
        <p><b>SINIMASUK&copy;2021</b></p>
        <p><b>YokaPrasMT_</b></p>
    </div>
    <script src="javaScript/scriptAll.js"></script>
</body>
</html>

<?php
    } else {
        echo "
        <script>
        alert('Maaf, Login terlebih dahulu...');
        document.location='LoginPage.php';
        </script>
        ";
    }
?>