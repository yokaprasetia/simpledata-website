<table class="tabelEdit">
    <tr>
        <th>Nomor</th>
        <th>Kode Barang</th>
        <th>Nama Barang</th>
        <th>Harga Barang</th>
        <th>Stok Barang</th>
        <th colspan="2">Aksi</th>
    </tr>
    <?php
        include "../koneksi.php";

        $no = 1;
        $data_item = mysqli_query($koneksi, "SELECT * FROM barang");
        
        if(isset($_GET['keyword'])){
            $data_item = mysqli_query($koneksi, "SELECT * FROM barang WHERE 
                kode_barang LIKE '%".$_GET['keyword']."%' OR
                nama_barang LIKE '%".$_GET['keyword']."%' OR
                harga_barang LIKE '%".$_GET['keyword']."%' OR
                stok_barang LIKE '%".$_GET['keyword']."%'
                ");
        }
        
        while ($tampil = mysqli_fetch_array($data_item)) {
    ?>
    <tr>
        <td><?php echo $no++; ?></td>
        <td><?php echo $tampil['kode_barang']; ?></td>
        <td><?php echo $tampil['nama_barang']; ?></td>
        <td><?php echo $tampil['harga_barang']; ?></td>
        <td><?php echo $tampil['stok_barang']; ?></td>
        <td class="edit"><a href="UpdateItemPage.php?kode_barang=<?php echo $tampil['kode_barang']; ?>">UPDATE</a></td>
        <td class="edit"><a href="DeleteItemPage.php?kode_barang=<?php echo $tampil['kode_barang']; ?>" 
        onclick="return confirm('Apakah Anda yakin ingin menghapus <?php echo $tampil['nama_barang']; ?> ?')">DELETE</a></td>
    </tr>
    <?php
        }
    ?>
</table>