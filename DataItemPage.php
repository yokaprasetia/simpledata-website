<?php
    include "koneksi.php";

    session_start();
    if (isset($_SESSION['user_id'])) {
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/stylesheet.css">
    <link rel="icon" href="gambar/gambar-logo.png">
    <title>SINIMASUK</title>
</head>
<body>
    <div class="content">
        <nav class="main">
            <div class="left-side">
                <ul>
                    <li><a class="menu" href=""><img src="gambar/gambar-menu.png" alt="">MENU</a>
                        <div class="submenu">
                            <ul>
                                <li class="sub-submenu"><a class="menu2" href="">HELP<img src="gambar/gambar-arrow.png" alt=""></a>
                                    <div class="submenu2">
                                        <ul>
                                            <li><a href="AboutUsPage.php">ABOUT US</a></li>
                                            <li><a href="CreditsPage.php">CREDITS</a></li>
                                        </ul>
                                    </div>
                                </li>
                                <li><a href="LogoutPage.php">LOGOUT</a></li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>

            <div class="right-side">
                <ul>
                    <li><a href="HomePage.php">HOME<span class="span-home"></a></li>
                    <li><a href="DataItemPage.php">DATA ITEM<span class="span-home"></a></li>
                    <li><a href="AddItemPage.php">ADD ITEM<span class="span-home"></a></li>
                    <li><a href="EditItemPage.php">EDIT ITEM<span class="span-home"></a></li>
                </ul>
            </div>

            <div class="menu-toggle">
                <input type="checkbox">
                <span></span>
                <span></span>
                <span></span>
            </div>

            <div class="garis"></div>
        </nav>

        <div class="belakang-nav"></div>

<!-- ============================BAGIAN EDIT START============================ -->

        <div class="isian-data">
            <div class="dataItem-page1">
                <h1>DATA BARANG SUPERMARKET 'SINIMASUK'</h1>
                <br>
            </div>
            <div class="dataItem-page2">
                <table class="tabel">
                    <tr>
                        <th>Nomor</th>
                        <th>Kode Barang</th>
                        <th>Nama Barang</th>
                        <th>Harga Barang</th>
                        <th>Stok Barang</th>
                    </tr>
                    <?php
                        $no = 1;
                        $data_item = mysqli_query($koneksi, "SELECT * FROM barang");
                        while ($tampil = mysqli_fetch_array($data_item)) {
                    ?>
                    <tr>
                        <td><?php echo $no++; ?></td>
                        <td><?php echo $tampil['kode_barang']; ?></td>
                        <td><?php echo $tampil['nama_barang']; ?></td>
                        <td><?php echo $tampil['harga_barang']; ?></td>
                        <td><?php echo $tampil['stok_barang']; ?></td>
                    </tr>
                    <?php
                        }
                    ?>
                </table>
            </div>
            <div class="dataItem-responsive">
                <table class="tabel">
                    <?php
                        $no = 1;
                        $data_item = mysqli_query($koneksi, "SELECT * FROM barang");
                        while ($tampil = mysqli_fetch_array($data_item)) {
                    ?>
                    <tr>
                        <th rowspan="4"><?php echo $no++; ?></th>
                        <th>Kode Barang</th>
                        <td colspan="2"><?php echo $tampil['kode_barang']; ?></td>
                    </tr>
                    <tr>
                        <th>Nama Barang</th>
                        <td colspan="2"><?php echo $tampil['nama_barang']; ?></td>
                    </tr>
                    <tr>
                        <th>Harga Barang</th>
                        <td colspan="2"><?php echo $tampil['harga_barang']; ?></td>
                    </tr>
                    <tr>
                        <th>Stok Barang</th>
                        <td colspan="2"><?php echo $tampil['stok_barang']; ?></td>
                    </tr>
                    <tr>
                        <td class="spasi" colspan="4">ini spasi</td>
                    </tr>
                    <?php
                        }
                    ?>
                </table>
            </div>
        </div>

<!-- ============================BAGIAN EDIT END============================ -->
    </div>

    <div class="bottom-side"></div>
    <div class="footer">
        <p><b>SINIMASUK&copy;2021</b></p>
        <p><b>YokaPrasMT_</b></p>
    </div>
    <script src="javaScript/scriptAll.js"></script>
</body>
</html>

<?php
    } else {
        echo "
        <script>
        alert('Maaf, Login terlebih dahulu...');
        document.location='LoginPage.php';
        </script>
        ";
    }
?>