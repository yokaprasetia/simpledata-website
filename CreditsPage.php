<?php
    include "koneksi.php";

    session_start();
    if (isset($_SESSION['user_id'])) {
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/stylesheet.css">
    <link rel="icon" href="gambar/gambar-logo.png">
    <title>SINIMASUK</title>
</head>
<body>
    <div class="content">
        <nav class="main">
            <div class="left-side">
                <ul>
                    <li><a class="menu" href=""><img src="gambar/gambar-menu.png" alt="">MENU</a>
                        <div class="submenu">
                            <ul>
                                <li class="sub-submenu"><a class="menu2" href="">HELP<img src="gambar/gambar-arrow.png" alt=""></a>
                                    <div class="submenu2">
                                        <ul>
                                            <li><a href="AboutUsPage.php">ABOUT US</a></li>
                                            <li><a href="CreditsPage.php">CREDITS</a></li>
                                        </ul>
                                    </div>
                                </li>
                                <li><a href="LogoutPage.php">LOGOUT</a></li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>

            <div class="right-side">
                <ul>
                    <li><a href="HomePage.php">HOME<span class="span-home"></a></li>
                    <li><a href="DataItemPage.php">DATA ITEM<span class="span-home"></a></li>
                    <li><a href="AddItemPage.php">ADD ITEM<span class="span-home"></a></li>
                    <li><a href="EditItemPage.php">EDIT ITEM<span class="span-home"></a></li>
                </ul>
            </div>

            <div class="menu-toggle">
                <input type="checkbox">
                <span></span>
                <span></span>
                <span></span>
            </div>

            <div class="garis"></div>
        </nav>

        <div class="belakang-nav"></div>

<!-- ============================BAGIAN EDIT START============================ -->

        <div class="isian-data">
            <div class="credits-page1">
                <h1>CREDITS</h1>
                <br>
            </div>
            <div class="credits-page2">
                <p>Dalam Era 4.0 atau Era Globalisasi saat ini, banyak benda-benda disekeliling kita yang telah 
                    memanfaatkan kecanggihan teknologi dalam kehidupan sehari - hari. Mulai dari bangun tidur hingga mau tidur, kita tidak luput 
                    dari kecanggihan teknologi. Namun perlahan demi perlahan, hal tersebut juga membawa danpak negatif bagi sebagian masyarakat 
                    yang kurang mengetahui manfaat optimal dari kecanggihan teknologi saat ini.</p>
                <p>Oleh karena itu,  diperlukan suatu usaha akitf terhadap masyarakat guna untuk meningkatkan kualitas Sumber 
                    Daya Manusia dalam berkehidupan seiring dengan jalannya era globalisasi ini, sehingga pada prakteknya akan memperoleh 
                    keuntungan dari adanya kecanggihan teknologi saat ini, bahkan membuat kehidupan sehari hari menjadi lebih mudah dan indah.</p>
                <p>Pada kesempatan kali ini, admin ingin memberikan suatu keuntungan dari adanya kecanggihan teknologi dalam 
                    kehidupan sehari-hari, yaitu dengan membuat suatu Database terkait barang yang dijual di suatu SUPERMARKET. Hal ini dilakukan 
                agar  dapat mendukung dan mempermudah dalam pengelolaan dan administrasi.</p>
            </div>
        </div>

<!-- ============================BAGIAN EDIT END============================ -->
    </div>

    <div class="bottom-side"></div>
    <div class="footer">
        <p><b>SINIMASUK&copy;2021</b></p>
        <p><b>YokaPrasMT_</b></p>
    </div>
    <script src="javaScript/scriptAll.js"></script>
</body>
</html> 

<?php
    } else {
        echo "
        <script>
        alert('Maaf, Login terlebih dahulu...');
        document.location='LoginPage.php';
        </script>
        ";
    }
?>