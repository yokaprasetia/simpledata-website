// Ambil Elemen yang dibutuhkan

var keyword = document.getElementById('keyword');
var container = document.getElementById('container');

// tambahkan even ketika keyword ditulis
keyword.addEventListener('keyup', function() {

    //buat object ajax
    var xhr = new XMLHttpRequest();

    // cek kesiapan ajax
    xhr.onreadystatechange = function() {
        if( xhr.readyState == 4 && xhr.status == 200 ) {
            container.innerHTML = xhr.responseText;
        }
    };
    
    //eksekusi ajax //
    // lokasi sama dengan **EditItemPage.php** //
    xhr.open('GET', 'ajax/AjaxPage.php?keyword=' + keyword.value, true);
    xhr.send();

});